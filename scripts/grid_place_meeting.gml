/// grid_place_meeting(x, y)
// This script will check if there's not a floor (wall or void, etc) where user wants to move.
var xx = argument[0];
var yy = argument[1];

// Remember our position.
var xp = x;
var yp = y;

// Update the position for the bbox calculations.
x = xx;
y = yy;

// Check for x meeting.
var x_meeting = (Level.grid[# bbox_right div CELL_WIDTH, bbox_top div CELL_HEIGHT]  != FLOOR) || 
                (Level.grid[# bbox_left div CELL_WIDTH, bbox_top div CELL_HEIGHT]   != FLOOR);
             
// Check for y meeting.   
var y_meeting = (Level.grid[# bbox_right div CELL_WIDTH, bbox_bottom div CELL_HEIGHT] != FLOOR) ||
                (Level.grid[# bbox_left div CELL_WIDTH, bbox_bottom div CELL_HEIGHT]  != FLOOR);
                
// To be a little safer, we can also check if the center point meets the tile.
var center_meeting = (Level.grid[# xx div CELL_WIDTH, yy div CELL_HEIGHT] != FLOOR);
                
// Move back.
x = xp;
y = yp;

// Return whether there is a place meeting or not.
// Check four corners and center of sprite.
// WARNING: If player sprite was huge, way bigger than the grid, then this could fail.
return x_meeting || y_meeting || center_meeting;
