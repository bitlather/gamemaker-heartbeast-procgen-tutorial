///get_path_to_player()

// Make sure player object is in the room
if (instance_exists(Player)) {

    var xx = (Player.x div CELL_WIDTH) * CELL_WIDTH + CELL_WIDTH / 2;
    var yy = (Player.y div CELL_HEIGHT) * CELL_HEIGHT + CELL_HEIGHT / 2;
    
    if (mp_grid_path(Level.grid_path, path, x, y, xx, yy, true)) { // return -1 if can't move there. returns 1 if can move there. also changes the path to update it if enemy has moved into it.
        path_start(path, 2, path_action_stop, false);
    }

}
